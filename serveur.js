const { v4 : uuidv4 } = require('uuid');
const express = require('express');
const fs = require('fs');
const app = express();
const port = 3000

app.set('view engine', 'pug');

app.use(express.json());
app.use(express.urlencoded({extended:true}));


app.get('/', (req,res) => res.send('API'))

app.get('/cities', function (req, res, next) {
    fs.readFile('cities.json', function (err, data){
        if (err) {
            res.status(404).send("Sorry can't find that!")
            next()
        } else {
            const myData = JSON.parse(data);
            res.render('index', {title: 'Données', data: myData.cities})
        }
    })
})

app.post('/city', function (req, res, next) {
    let myData;
    const NewCity = req.body.name
    fs.readFile('cities.json', function (err, data){
        if (err) {
            myData = {"cities":[]}
        } else {
            myData = JSON.parse(data)
        }
        
        for (city of myData.cities) {
            if (city.name == NewCity){ 
                res.status(500).send("Sorry this city already exist!")
                next();
            };
        } 
        myData.cities.push({"id" : uuidv4(), "name" : NewCity})
        fs.writeFile('cities.json', JSON.stringify(myData), (err) => {})
        res.status(200).send("The new city has been add!")
        next()
    })
})

app.put('/city', function (req, res, next) {
    const Id = req.body.id
    const newName = req.body.name
    fs.readFile('cities.json', function (err, data) {
        if (err) {
            res.status(404).send("Sorry can't find 'cities.json'!")
            next()
        }
        const myData = JSON.parse(data)
        for (city of myData.cities) {
            if (city.id == Id){ 
                city.name = newName
                fs.writeFile('cities.json', JSON.stringify(myData), (err) => {})
                res.status(200).send("The city name has been change!")
                next()
                return
            }
        } 
        res.status(404).send("Sorry can't find this city!")
        next();
    })
})

app.delete('/city/:id', function (req, res, next) {
    const DelId = req.params.id
    fs.readFile('cities.json', function (err, data) {
        if (err) {
            res.status(404).send("Sorry can't find 'cities.json'!")
            next()
        }
        const myData = JSON.parse(data)
        myData.cities = myData.cities.filter(city => city.id !== DelId )
        fs.writeFile('cities.json', JSON.stringify(myData), (err) => {})
        res.status(200).send("The city has been Deleted!")
        next()
    })
})


app.listen(port, () => console.log(`Server listening on port ${port}!`))


